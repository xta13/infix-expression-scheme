(define make-empty-env
    (lambda ()
        (list)
    )
)

(define apply-env
    (lambda (env v)
        (cond
            ((null? env)(error "unknown variable" v))
            ((equal? (car(car env)) v) (car (cdr(car env)))) 
            (else (apply-env (cdr env) v))
        )
    )
)

(define extend-env
    (lambda (v val env)
        (let (
                (env_without_v (remove_v_from_env v env))
             )
            (cons (list v val) env_without_v)
        )
    )
)

(define remove_v_from_env
    (lambda (v env)
        ( cond
            ((null? env) '())
            ((equal? (car (car env)) v) (cdr env)) 
            (else (cons (car env) (remove_v_from_env v (cdr env))))
        )
    )
)


;logic learned from CMPT383 lecture slide: http://www.cs.sfu.ca/CourseCentral/383/tjd/scheme-intro.html
; use of expt function learned from https://www.cs.indiana.edu/scheme-repository/R4RS/r4rs_8.html

(define myeval
    (lambda (expr env)
        (cond
            ((number? expr) expr)
            ((symbol? expr) (apply-env env expr))
            ((and (list? expr) (equal? (car expr) 'inc) (= (length expr) 2 )) (+ (myeval (car (cdr expr)) env) 1))
            ((and (list? expr) (equal? (car expr) 'inc) (not (= (length expr) 2 ))) (error "wrong number of operand for inc operation"))  
            ((and (list? expr) (equal? (car expr) 'dec) (= (length expr) 2 )) (- (myeval (car (cdr expr)) env) 1))
            ((and (list? expr) (equal? (car expr) 'dec) (not (= (length expr) 2 ))) (error "wrong number of operand for dec operation"))  
            ((and (list? expr) (= (length expr) 3))
                (let ((left (myeval (car expr ) env))
                      (op (car (cdr expr)))
                      (right (myeval (car (cdr (cdr expr))) env))
                     )
                    (cond
                        ((equal? op '+) (+ left right))
                        ((equal? op '-) (- left right))
                        ((equal? op '*) (* left right))
                        ((and (equal? op '/) (not (equal? right 0))) (/ left right))
                        ((and (equal? op '/) (equal? right 0)) (error "division by 0 detected"))
                        ((equal? op '**) (expt left right))
                        (else (error "unknown operation detected"))
                    )
                )
            )
            (else 
                (error "unknown expression detected")
            )
        )
    )
)